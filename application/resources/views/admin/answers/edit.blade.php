@extends('layouts.app')

@section('content')

    <h1>Edit Answer</h1>

    {{ Form::model($answer, array('route' => array('answers.update', $answer->id), 'method' => 'PUT')) }}
        {{ csrf_token() }}
        <div class="row large-12 columns">
        {!! Form::label('answer', 'Answer:') !!}
        {!! Form::text('answer', $answer->answer, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Edit Answer', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
