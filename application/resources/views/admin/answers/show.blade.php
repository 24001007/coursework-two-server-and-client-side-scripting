@extends('layouts.app')

@section('content')

@if (isset ($answer))
<h1>{{ $answer->answer }}</h1>
<p>Belongs to question: {{$answer->question->question}}</p>

<p>Amount of answers: {{ count($answer->responses) }}</p>

<a href="/admin/answers/{{ $answer->id }}/edit" name="{{ $answer->answer }}" class="button">Edit answer</a>

{{ Form::open(['action' => 'AnswerController@index', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Back to answers', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endif

@endsection
