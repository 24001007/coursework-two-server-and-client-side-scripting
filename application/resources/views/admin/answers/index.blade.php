@extends('layouts.app')

@section('content')

<h1>Answers</h1>

<section>
    @if (isset ($answers))

        <ul>
            @foreach ($answers as $answer)
            <li><a href="/admin/answers/{{ $answer->id }}" name="{{ $answer->answer }}">{{ $answer->answer }}</a></li>
            @endforeach
        </ul>
    @else
        <p> No answers added yet </p>
    @endif
</section>

{{ Form::open(['action' => 'AnswerController@create', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Add answer', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endsection
