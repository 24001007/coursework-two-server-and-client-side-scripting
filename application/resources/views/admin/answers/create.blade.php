@extends('layouts.app')

@section('content')

    <h1>Add Answer</h1>

    {!! Form::open(['action' => 'AnswerController@store', 'id' => 'createanswer']) !!}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('answer', 'Answer:') !!}
        {!! Form::text('answer', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('question', 'Question:') !!}
        {!! Form::select('question[]', $questions, null,['class' => 'large-8 columns', 'multiple']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Answer', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
