@extends('layouts.app')

@section('content')

@if (isset ($question))
<h1>{{ $question->question }}</h1>
<p>Belongs to questionnaire: <strong>{{$question->questionnaire->title}}</strong></p>

<h2>Answers:</h2>

<ul>
    @foreach ($question->answers as $answer)
    <li name="{{ $answer->answer }}">{{ $answer->answer }}</a></li>
    @endforeach
</ul>

<a href="/admin/questions/{{ $question->id }}/edit" name="{{ $question->title }}" class="button">Edit Question</a>

{{ Form::open(['action' => 'QuestionController@index', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Back to questions', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endif

@endsection
