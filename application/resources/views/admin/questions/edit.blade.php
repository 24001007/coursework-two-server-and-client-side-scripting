@extends('layouts.app')

@section('content')

    <h1>Edit Question</h1>

    {{ Form::model($question, array('route' => array('questions.update', $question->id), 'method' => 'PUT')) }}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question:') !!}
        {!! Form::text('question', $question->question, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Edit Question', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
