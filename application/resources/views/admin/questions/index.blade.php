@extends('layouts.app')

@section('content')

<h1>Questions</h1>

<section>
    @if (isset ($questions))

        <ul>
            @foreach ($questions as $question)
            <li><a href="/admin/questions/{{ $question->id }}" name="{{ $question->question }}">{{ $question->question }}</a></li>
            @endforeach
        </ul>
    @else
        <p> No questions added yet </p>
    @endif
</section>

{{ Form::open(['action' => 'QuestionController@create', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endsection
