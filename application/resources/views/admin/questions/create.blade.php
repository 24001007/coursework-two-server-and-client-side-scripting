@extends('layouts.app')

@section('content')

    <h1>Add Question</h1>

    {!! Form::open(['action' => 'QuestionController@store', 'id' => 'createquestion']) !!}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('question', 'Question:') !!}
        {!! Form::text('question', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('questionnaire', 'Questionnaire:') !!}
        {!! Form::select('questionnaire[]', $questionnaires, null,['class' => 'large-8 columns', 'multiple']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
