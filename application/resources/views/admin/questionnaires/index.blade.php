@extends('layouts.app')

@section('content')
<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
            <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no questionnaires added yet </p>
    @endif
</section>

{{ Form::open(['action' => 'QuestionnaireController@create', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endsection
