@extends('layouts.app')

@section('content')

@if (isset ($questionnaire))
<h1>{{ $questionnaire->title }}</h1>

@if ($questionnaire->ethics_statement != '')
<h2>Ethics Statement:</h2>
<p>{{$questionnaire->ethics_statement}}</p>
@endif

<h2>Is Public?</h2>
<p>Is Public: {{$questionnaire->is_public ? "Yes" : "No"}}</p>

@if (count($questionnaire->questions) > 0)
<h2>Questions:</h2>

<ol>
    @foreach ($questionnaire->questions as $question)
    <li name="{{ $question->question }}">
        {{ $question->question }}
        @if (isset ($question->answers))
        <p>Possible Answers:</p>
        <ul>
            @foreach ($question->answers as $answer)
            <li>{{$answer->answer}}</li>
                @if (isset ($answer->responses))
                <ul>
                    <p>Responses: {{ count($answer->responses) }}</p>
                </ul>
                @endif

            @endforeach
        </ul>
        @endif
    </li>
    @endforeach
</ol>
@endif

<a href="/admin/questionnaires/{{ $questionnaire->id }}/edit" name="{{ $questionnaire->title }}" class="button">Edit Questionnaire</a>

{{ Form::open(['action' => 'QuestionnaireController@index', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Back to questionnaires', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endif

@endsection
