@extends('layouts.app')

@section('content')
    <h1>Add Questionnaire</h1>

    {!! Form::open(['action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire']) !!}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('ethics_statement', 'Ethics Statement:') !!}
        {!! Form::text('ethics_statement', null, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
