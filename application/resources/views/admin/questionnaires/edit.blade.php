@extends('layouts.app')

@section('content')
    <h1>Edit Questionnaire</h1>

    {{ Form::model($questionnaire, array('route' => array('questionnaires.update', $questionnaire->id), 'method' => 'PUT')) }}
        {{ csrf_token() }}
    <div class="row large-12 columns">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', $questionnaire->title, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('ethics_statement', 'Ethics Statement:') !!}
        {!! Form::text('ethics_statement', $questionnaire->ethics_statement, ['class' => 'large-8 columns']) !!}
    </div>

    <div class="row large-12 columns">
        {!! Form::label('is_public', 'Is Public?:') !!}
        {!! Form::checkbox('is_public', $questionnaire->is_public) !!}
    </div>

    <div class="row large-4 columns">
        {!! Form::submit('Edit Questionnaire', ['class' => 'button']) !!}
    </div>
    {!! Form::close() !!}

@endsection
