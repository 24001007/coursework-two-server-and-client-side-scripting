@extends('layouts.app')

@section('content')

@if (isset ($questionnaire))
<h1>{{ $questionnaire->title }}</h1>

<h2>Ethics Statement:</h2>
<p>{{$questionnaire->ethics_statement}}</p>


@if (isset ($questionnaire->questions))
<h2>Questions:</h2>

<ol>
    @foreach ($questionnaire->questions as $question)
    <li name="{{ $question->question }}">
        <a href="respond/{{ $question->id }}">{{ $question->question }}</a>
    </li>
    @endforeach
</ol>
@else
<p>No questions have been added</p>
@endif

{{ Form::open(['action' => 'QuestionnaireController@index', 'method' => 'get']) }}
    <div class="row">
        {!! Form::submit('Back to questionnaires', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endif

@endsection
