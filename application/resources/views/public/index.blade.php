@extends('layouts.app')

@section('content')

<h1>Public Questionnaires</h1>

<section>
    @if (isset ($questionnaires))

        <ul>
            @foreach ($questionnaires as $questionnaire)
            <li><a href="/surveys/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li>
            @endforeach
        </ul>
    @else
        <p> no public questionnaires added yet </p>
    @endif
</section>

@endsection
