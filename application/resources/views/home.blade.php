@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="nav">
                    <li class="nav-item">
                            <a href="/admin/questionnaires" class="btn btn-light nav-link">Questionnaires</a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/questions" class="btn btn-light nav-link">Questions</a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/answers" class="btn btn-light nav-link">Answers</a>
                        </li>
                        <li class="nav-item">
                            <a href="/public" class="btn btn-light nav-link">Public Questionnaires</a>
                        </li>
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
