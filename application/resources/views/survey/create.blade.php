@extends('layouts.app')

@section('content')

<h1>{{ $questionnaire->title }}</h1>

<h2>Ethics Statement:</h2>
<p>{{$questionnaire->ethics_statement}}</p>


{{ Form::open(['url' => ['/surveys', $questionnaire->id], 'id' => 'answerquestionnaire']) }}
    {{ csrf_token() }}
    @foreach ($questionnaire->questions as $key => $question)
        <article>
            <header><strong>{{$key + 1}}. </strong>{{$question->question}}</header>
            <ul>
                @foreach ($question->answers as $answer)
                <label for="answer{{ $answer->id }}">
                    <li>
                        <input type="radio" name="responses[{{ $key }}][answer_id]" id="answer{{ $answer->id }}" value="{{ $answer->id }}">
                        {{ $answer->answer }}

                        <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{ $question->id }}">
                        <input type="hidden" name="responses[{{ $key }}][questionnaire_id]" value="{{ $questionnaire->id }}">
                    </li>
                </label>
                @endforeach
            </ul>
        </article>
    @endforeach


    <div class="row large-4 columns">
        {!! Form::submit('Submit', ['class' => 'button']) !!}
    </div>
{{ Form::close() }}

@endsection
