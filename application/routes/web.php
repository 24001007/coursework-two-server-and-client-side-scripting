<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'HomeController');
Route::resource('/public', 'PublicController');
Route::get('/surveys/{questionnaire}', 'SurveyController@create');
Route::post('/surveys/{questionnaire}', 'SurveyController@store');

Route::group(['middleware' => ['web']], function () {
    //Auth::routes();
    Route::auth();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/admin/questionnaires', 'QuestionnaireController');
    Route::resource('/admin/questions', 'QuestionController');
    Route::resource('/admin/answers', 'AnswerController');
});


