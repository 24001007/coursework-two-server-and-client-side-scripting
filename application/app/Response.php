<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'response',
    ];


    /**
     * A response belongs to a single answer
     */
    public function answer() {
        return $this->belongsTo(Answer::class);
    }
}
