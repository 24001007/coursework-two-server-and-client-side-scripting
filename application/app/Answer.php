<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'answer',
    ];

    /**
     * An answer belongs to a single question
     */
    public function question() {
        return $this->belongsTo(Question::class);
    }

    /**
     * An answer has (zero or) many responses
     */
    public function responses() {
        return $this->hasMany(Response::class);
    }
}
