<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\Question;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the answers
        $answers = Answer::all();
        return view('admin/answers/index', ['answers' => $answers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve all the questions
        $questions = Question::pluck('question', 'id');

        // return the data with the view
        return view('admin/answers/create', compact('questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Find the question in the database, that was selected in the form
        $question = Question::where('id', $request->input('question'))->first();

        // Create an answer to that question
        $answer = $question->answers()->create($request->all());

        // Return to the answers index page
        return redirect('admin/answers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaire
        $answer = Answer::where('id', $id)->first();

        // if questionnaire does not exist return to index
        if (!$answer) {
            return redirect('/admin/answers');
        }

        // Show the view for a single answer
        return view('/admin/answers/show', compact('answer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get the Answer
        $answer = Answer::find($id);

        // Show the form for editing
        return view('admin/answers/edit', compact('answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Find the current answer
        $answer = Answer::find($id);
        // Find the question that the answer belongs to
        $question = Question::where('id', $answer->question->id)->first();

        // Store all the form data in the $data variable
        $data = request()->all();

        // Update the answer
        $answer = $question->answers()->where('id', $answer->id)->update(['answer' => $data["answer"]]);

        // Save the changes
        $question->save();

        // Redirect the user to the index page for answers
        return redirect('admin/answers/index');
    }
}
