<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Answer;

class SurveyController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Questionnaire $questionnaire)
    {
        // Lazily load all the questions and answers for the questionnaire
        $questionnaire->load('questions.answers');
        return view('survey.create', compact('questionnaire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // For each response to a question in the form
        foreach (request()->all()["responses"] as $response) {
            // Find the answer that the response (will) belongs to
            $answer = Answer::where('id', $response["answer_id"])->first();
            // Create the response for the answer
            $response = $answer->responses()->create();
        }
        return redirect('/public');
    }
}
