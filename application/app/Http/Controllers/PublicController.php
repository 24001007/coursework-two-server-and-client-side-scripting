<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the questionnaires
        $questionnaires = Questionnaire::where('is_public', 1)->get();

        return view('public/index', ['questionnaires' => $questionnaires]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaire
        $questionnaire = Questionnaire::where('id', $id)->where('is_public', 1)->first();

        // if questionnaire does not exist return to index
        if (!$questionnaire) {
            return redirect('/public/index');
        }
        return view('public/show', compact('questionnaire'));
    }
}
