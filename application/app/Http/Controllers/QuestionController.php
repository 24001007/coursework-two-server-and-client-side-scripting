<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the questions
        $questions = Question::all();
        return view('admin/questions/index', ['questions' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Retrieve all the questionnaires
        $questionnaires = Questionnaire::pluck('title', 'id');

        // return the data with the view
        return view('admin/questions/create', compact('questionnaires'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get the questionnaire that was selected in the form
        $questionnaire = Questionnaire::where('id', $request->input('questionnaire'))->first();

        // Create a question to that questionnaire
        $question = $questionnaire->questions()->create($request->all());

        return redirect('admin/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaire
        $question = Question::where('id', $id)->first();

        // if questionnaire does not exist return to index
        if (!$question) {
            return redirect('/admin/questions');
        }
        return view('/admin/questions/show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get the Question
        $question = Question::find($id);

        // Show the form for editing
        return view('admin/questions/edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Get the question in the database
        $question = Question::find($id);

        // Find the questionnaire that the question belongs to
        $questionnaire = Questionnaire::where('id', $question->questionnaire->id)->first();

        // Store the form data in the $data variable
        $data = request()->all();

        // Update the question
        $question = $questionnaire->questions()->where('id', $question->id)->update(['question' => $data["question"]]);

        // Save the changes
        $questionnaire->save();

        return redirect('admin/questions/index');
    }
}
