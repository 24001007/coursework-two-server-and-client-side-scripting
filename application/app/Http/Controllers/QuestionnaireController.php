<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\Question;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the questionnaires
        $questionnaires = Questionnaire::all();

        return view('admin/questionnaires/index', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/questionnaires/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get all the data from the form and store it in $data
        $data = $request->all();

        // Create the questionnaire
        $questionnaire = auth()->user()->questionnaires()->create($data);

        return redirect('admin/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the questionnaire
        $questionnaire = Questionnaire::where('id', $id)->first();

        // if questionnaire does not exist return to index
        if (!$questionnaire) {
            return redirect('/admin/questionnaires');
        }
        return view('/admin/questionnaires/show', compact('questionnaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get the Questionnaire
        $questionnaire = Questionnaire::find($id);

        // Show the form for editing
        return view('admin/questionnaires/edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Get the questionnaire from the database
        $questionnaire = Questionnaire::find($id);

        // Store the form data in the $data variable
        $data = request()->all();

        // Handle setting publicity
        if (isset($data["is_public"])) {
            // Update the is_public to true if "is_public" is set on the form
            $questionnaire->is_public = 1;
        } else {
            // Update the is_public to false if "is_public" is not set on the form
            $questionnaire->is_public = 0;
        }

        // Update the title
        $questionnaire->title = $data["title"];

        // Update the ethics statement
        $questionnaire->ethics_statement = $data["ethics_statement"];

        // Save the changes to the database
        $questionnaire->save();

        return view('admin/questionnaires/show', compact('questionnaire'));
    }
}
