<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'title',
        'ethics_statement'
    ];

    /**
     * A questionnaire has (zero or) many questions
     */
    public function questions() {
        return $this->hasMany(Question::class);
    }


    /**
     * A questionnaire belongs to a single user
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
