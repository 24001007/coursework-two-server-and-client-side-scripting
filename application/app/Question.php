<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question'
    ];


    /**
     * A question belongs to a single questionnaire
     */
    public function questionnaire() {
        return $this->belongsTo(Questionnaire::class);
    }

    /**
     * A question has (zero or) many possible answers
     */
    public function answers() {
        return $this->hasMany(Answer::class);
    }
}
