<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeding commented out. Was only used for testing.
        // DB::table('questionnaires')->insert([
        //   ['id'=>1,
        //    'title'=>'Best Frameworks',
        //    'is_public'=>true,
        //    'ethics_statement'=> ''
        //   ],
        //   ['id'=>2,
        //    'title'=>'Best Dogs',
        //    'is_public'=>false,
        //    'ethics_statement'=> ''
        //   ],
        //   ['id'=>3,
        //    'title'=>'Best Database',
        //    'is_public'=>true,
        //    'ethics_statement'=> ''
        //   ],
        // ]);
    }
}
