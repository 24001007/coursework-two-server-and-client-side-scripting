<?php
$I = new FunctionalTester($scenario);

$I->am('a admin');
$I->wantTo('test Laravel Working');

// When
$I->amOnPage('/');

// Then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel', '.title');
