<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new questionnaire');

// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->dontSee('RandomQuestionnaire');

// And
$I->click('Add Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires/create');

// And
$I->see('Add Questionnaire', 'h1');
$I->submitForm('#createquestionnaire', [
    'title' => 'RandomQuestionnaire',
]);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('New questionnaire added!');
$I->see('RandomQuestionnaire');




/*
// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);

// Add a test questionnaire to check that content can be seen in list at start
$I->haveRecord('questionnaires', [
    'id' => '9000',
    'researcherID' => '9999',
    'title' => 'Questionnaire 1',
    'ethicsStatement' => 'This is an ethics statement',
    'isPublic' => false
]);
*/
