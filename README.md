# Questionnaire Web-Application for CW2 in Server and Client Side Scripting

- The `application/` folder contains the application
- The `Modelling/` folder contains all the models produced
- The `Normalisation/` folder contains the files used in the normalisation
  process
- The `Written Use Cases/` folder contains the written use cases.
